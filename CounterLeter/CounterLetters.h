#ifndef EXAMSEBEK_COUNTERLETTERS_H
#define EXAMSEBEK_COUNTERLETTERS_H

#include <map>
#include <string>
#include <vector>
#include <istream>

class CounterLetters {
public:
    CounterLetters(const std::string& letters);
    std::map<char, size_t> countLetters() const;
    /*
     * stream should be valid
     */
    virtual void setInputStream(std::istream& stream);
private:
    std::istream* m_stream;
    std::vector<char> m_lettersToCount;
};


#endif //EXAMSEBEK_COUNTERLETTERS_H
