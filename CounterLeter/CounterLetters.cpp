#include "CounterLetters.h"
#include <algorithm>
#include <cassert>

CounterLetters::CounterLetters(const std::string &letters):m_stream(nullptr), m_lettersToCount(letters.begin(), letters.end()) {
}

std::map<char, size_t> CounterLetters::countLetters() const {
    std::map<char, size_t> result;
    std::transform(m_lettersToCount.cbegin(), m_lettersToCount.cend(),
                   std::inserter(result, result.begin()), [](char c) -> std::pair<char, size_t>{
        return {c, 0};
    });

    assert(m_stream);

    std::string line;
    while(!m_stream->fail()){
        std::getline(*m_stream, line);
        for (auto& ch : line){
            if (auto it = result.find(ch); it!=result.end()){
                it->second++;
            }
        }
        line.clear();
    }

    return result;
}

void CounterLetters::setInputStream(std::istream &stream) {
    m_stream = &stream;
}
