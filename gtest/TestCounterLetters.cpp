#include "CounterLetters.h"

#include <fstream>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

TEST(TestCounterLetters, TestEmptyCharString){
    CounterLetters counterLetters("");
    std::ifstream file;
    file.open("../../testData/testData1");
    ASSERT_TRUE(file.is_open());
    counterLetters.setInputStream(file);
    EXPECT_TRUE(counterLetters.countLetters().empty());
}

TEST(TestCounterLetters, TestNotEmptyCharString){
    CounterLetters counterLetters("iea");
    std::ifstream file;
    file.open("../../testData/testData2");
    ASSERT_TRUE(file.is_open());
    counterLetters.setInputStream(file);
    std::map<char, size_t> expectedMap{{'i', 0}, {'a', 0}, {'e', 0}};
    EXPECT_EQ(counterLetters.countLetters(),  expectedMap);
}

TEST(TestCounterLetters, TestSuccsess){
    CounterLetters counterLetters("iea");
    std::ifstream file;
    file.open("../../testData/testData1");
    ASSERT_TRUE(file.is_open());
    counterLetters.setInputStream(file);
    std::map<char, size_t> expectedMap{{'i', 6}, {'a', 6}, {'e', 11}};
    EXPECT_EQ(counterLetters.countLetters(),  expectedMap);
}


