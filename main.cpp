#include <iostream>
#include <fstream>
#include "CounterLetters.h"

int main()
{
    std::ifstream file;
    file.open("../testData/testData1");
    if (!file.is_open()){
        return 0;
    }

    CounterLetters counterLetters("aie");
    counterLetters.setInputStream(file);
    auto res = counterLetters.countLetters();

    for (auto& it: res){
        std::cout<<"For letter: ["<<it.first<<"] count = "<<it.second<<"\n";
    }
    return 0;
}
